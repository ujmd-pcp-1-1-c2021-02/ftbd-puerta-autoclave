﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void btnAbrir_Click(object sender, EventArgs e)
        {
            tmrAbrir.Enabled = true;
            tmrCerrar.Enabled = false;
        }

        private void btnCerrar_Click(object sender, EventArgs e)
        {
            tmrAbrir.Enabled = false;
            tmrCerrar.Enabled = true;
        }

        private void tmrAbrir_Tick(object sender, EventArgs e)
        {
            if (panelPuerta.Height >= 5)
            {
                panelPuerta.Height -= 5;
                panelPuerta.Top += 5;
            }

        }

        private void tmrCerrar_Tick(object sender, EventArgs e)
        {
            if (panelPuerta.Height <= 175 )
            {
                panelPuerta.Height += 5;
                panelPuerta.Top -= 5;
            }
        }

        private void label2_Click(object sender, EventArgs e)
        {

        }
    }
}
